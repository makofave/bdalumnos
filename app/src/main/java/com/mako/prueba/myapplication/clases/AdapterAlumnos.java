package com.mako.prueba.myapplication.clases;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mako.prueba.myapplication.R;
import com.mako.prueba.myapplication.fragment.InsertFragment;

import java.util.List;

/**
 * Created by Desarrollo 1 on 01/02/2018.
 */

public class AdapterAlumnos extends RecyclerView.Adapter<AdapterAlumnos.ViewHolder> {

    public Alumno alumnoSelect;
    public int globalPosition;
    private Context context;
    private List<Alumno> my_data;
    private  OnFragmentInteractionListener mListener;

    public AdapterAlumnos(Context context, List<Alumno>my_data){
        this.context=context;
        this.my_data=my_data;
        mListener = (OnFragmentInteractionListener) context;


    }
    public Alumno getAlumnoSelect(int i)
    {
        if(i>0)
            globalPosition=-1;
        return alumnoSelect;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_alumno,parent,false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if((alumnoSelect!=null)&&position==globalPosition)
        {
            holder.cardView.setCardBackgroundColor(Color.CYAN);

        }
        else
        {
            //revert back to regular color
            holder.cardView.setCardBackgroundColor(Color.WHITE);
        }
        holder.tvId.setText(""+my_data.get(position).getId());
        holder.tvNombre.setText(""+my_data.get(position).getNombre());
        holder.tvApPaterno.setText(""+my_data.get(position).getAp_Paterno());
        holder.tvApMaterno.setText(""+my_data.get(position).getAp_Materno());
        holder.tvFecha.setText(""+my_data.get(position).getFecha_NacimientoString());
    }



    @Override
    public int getItemCount() {
        return my_data.size();
    }
    public void setList(List<Alumno> list)
    {
        this.my_data=list;

    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView tvId;
        public TextView tvNombre;
        public TextView tvApPaterno;
        public TextView tvApMaterno;
        public TextView tvFecha;
        public CardView cardView;


        public ViewHolder(final View itemView) {
            super(itemView);
            //card_view_Alumnos

            cardView=(CardView)itemView.findViewById(R.id.cardViewAlumnos);
            tvId=(TextView)itemView.findViewById(R.id.txtId);
            tvNombre=(TextView)itemView.findViewById(R.id.txtNombre);
            tvApPaterno=(TextView)itemView.findViewById(R.id.txtApPaterno);
            tvApMaterno=(TextView)itemView.findViewById(R.id.txtApMaterno);
            tvFecha=(TextView)itemView.findViewById(R.id.txtFecha);

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.seleccionado(my_data.get(getAdapterPosition()));
                    globalPosition=getAdapterPosition();
                    alumnoSelect=my_data.get(getAdapterPosition());
                    notifyDataSetChanged();

                }
            });


        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void seleccionado(Alumno alumno);
    }


}
