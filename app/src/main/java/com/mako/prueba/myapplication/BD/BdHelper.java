package com.mako.prueba.myapplication.BD;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Makofave on 16/09/2019.
 */

public class BdHelper extends SQLiteOpenHelper {
    private static final String BD_NAME = "alumnos.sqlite";
    private static final int BD_VERSION = 1;

    public BdHelper(Context context)
    {
        super(context,BD_NAME,null,BD_VERSION);

    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(BdManager.Create_Table_Alumno);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
