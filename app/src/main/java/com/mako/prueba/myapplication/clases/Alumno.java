package com.mako.prueba.myapplication.clases;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Makofave on 14/09/2019.
 */

public class Alumno
{
    private int id;
    private String nombre;
    private String ap_Paterno;
    private String ap_Materno;
    private Date fecha_Nacimiento;
    private Boolean estatus;

    public Alumno(String nombre, String ap_Paterno, String ap_Materno) {
        this.nombre = nombre;
        this.ap_Paterno = ap_Paterno;
        this.ap_Materno = ap_Materno;
        this.estatus=true;
        this.id=-1;

    }

    public Alumno(String nombre, String ap_Paterno, String ap_Materno, Date fecha_Nacimiento) {
        this.nombre = nombre;
        this.ap_Paterno = ap_Paterno;
        this.ap_Materno = ap_Materno;
        this.fecha_Nacimiento = fecha_Nacimiento;
        this.estatus=true;
        this.id=-1;
    }

    public Alumno(String nombre, String ap_Paterno, String ap_Materno, String fecha_Nacimiento) {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaDate = null;
        try {
            fechaDate = formato.parse(fecha_Nacimiento);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.nombre = nombre;
        this.ap_Paterno = ap_Paterno;
        this.ap_Materno = ap_Materno;
        this.fecha_Nacimiento = fechaDate;
        this.estatus=true;
        this.id=-1;
    }


    public String obtenerNombre() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return "Alumno{" +
                "id='" + id + '\'' +
                "nombre='" + nombre + '\'' +
                ", ap_Paterno='" + ap_Paterno + '\'' +
                ", ap_Materno='" + ap_Materno + '\'' +
                ", fecha_Nacimiento=" + dateFormat.format(fecha_Nacimiento)+
                ", estatus=" + estatus +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAp_Paterno() {
        return ap_Paterno;
    }

    public void setAp_Paterno(String ap_Paterno) {
        this.ap_Paterno = ap_Paterno;
    }

    public String getAp_Materno() {
        return ap_Materno;
    }

    public void setAp_Materno(String ap_Materno) {
        this.ap_Materno = ap_Materno;
    }

    public Date getFecha_Nacimiento() {
        return fecha_Nacimiento;
    }
    public String getFecha_NacimientoString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.format(fecha_Nacimiento);
    }

    public void setFecha_Nacimiento(Date fecha_Nacimiento) {
        this.fecha_Nacimiento = fecha_Nacimiento;
    }

    public Boolean getEstatus() {
        return estatus;
    }

    public void setEstatus(Boolean estatus) {
        this.estatus = estatus;
    }
}
