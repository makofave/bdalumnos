package com.mako.prueba.myapplication;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.mako.prueba.myapplication.BD.BdManager;
import com.mako.prueba.myapplication.clases.AdapterAlumnos;
import com.mako.prueba.myapplication.clases.Alumno;
import com.mako.prueba.myapplication.fragment.InsertFragment;

import java.util.List;

public class MainActivity extends AppCompatActivity implements InsertFragment.OnFragmentInteractionListener,AdapterAlumnos.OnFragmentInteractionListener {


   public  Button nuevo,actualizar,eliminar;
   public RecyclerView recyclerViewAlumnos;
   public AdapterAlumnos adapterAlumnos;
   public BdManager bdManager;
   public List<Alumno> listAlumnos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainn);
        bdManager=new BdManager(MainActivity.this);
        listAlumnos=bdManager.listaCap();
        setupView();




    }
    public void setupView()
    {
        recyclerViewAlumnos=(RecyclerView)findViewById(R.id.recycleViewAlumnos);

        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerViewAlumnos.setLayoutManager(manager);
        recyclerViewAlumnos.setHasFixedSize(true);
        adapterAlumnos=new AdapterAlumnos(MainActivity.this,listAlumnos);
        recyclerViewAlumnos.setAdapter(adapterAlumnos);



        nuevo=(Button)findViewById(R.id.btnCrear);
        eliminar=(Button)findViewById(R.id.btnEliminar);
        actualizar=(Button)findViewById(R.id.btnActualizar);

        actualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Alumno alumno=adapterAlumnos.getAlumnoSelect(0);
                if(alumno!=null)
                {
                    InsertFragment insertFragment=new InsertFragment();
                    insertFragment.setAlumno(alumno);
                    insertFragment.show(getFragmentManager(),"");
                    //bdManager.insertAlumno(alumno);
                    //updateLista();
                }
                else
                    Toast.makeText(getApplication(), "Seleccione Un Alumno", Toast.LENGTH_SHORT).show();
            }
        });
        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Alumno alumno=adapterAlumnos.getAlumnoSelect(1);
               if(alumno!=null)
               {
                   bdManager.eliminarAlumno(""+alumno.getId());
                   updateLista();
               }
               else
                   Toast.makeText(getApplication(), "Seleccione Un Alumno", Toast.LENGTH_SHORT).show();


            }
        });

        nuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                InsertFragment insertFragment=new InsertFragment();

                insertFragment.show(getFragmentManager(),"");

            }
        });
    }






    @Override
    public void onFragmentInteraction(Uri uri) {
        //Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();


    }

    @Override
    public void seleccionado(Alumno alumno) {
        //Toast.makeText(this, "Success"+alumno.obtenerNombre(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateLista() {

            adapterAlumnos.setList(bdManager.listaCap());
            adapterAlumnos.notifyDataSetChanged();

    }



}
