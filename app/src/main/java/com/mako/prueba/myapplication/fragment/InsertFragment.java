package com.mako.prueba.myapplication.fragment;

import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.mako.prueba.myapplication.BD.BdManager;
import com.mako.prueba.myapplication.R;
import com.mako.prueba.myapplication.clases.Alumno;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InsertFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InsertFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InsertFragment extends DialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    public EditText nombre,ap_Pat,ap_Mat,fecha;
    public Button mostrar,guardar;
    BdManager bdManager;
    public View view;
    public Alumno alumno;

    @Override
    public void onStart() {
        super.onStart();
        try
        {
            mListener = (OnFragmentInteractionListener) getActivity();
            mListener.onFragmentInteraction(Uri.parse("doWhatYouWant"));
            Log.d("EST12","Entro al Attach");

        }catch (ClassCastException e) {
            Log.d("EST12",e.toString());
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnFragmentInteractionListener");
        }



    }
    public void setAlumno(Alumno alumno){
        this.alumno=alumno;

    }
    public InsertFragment()
    {

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment InsertFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InsertFragment newInstance(String param1, String param2) {
        InsertFragment fragment = new InsertFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=  inflater.inflate(R.layout.fragment_insertt, container, false);

        bdManager=new BdManager(getActivity());
        setupView();
        if(alumno!=null)
        {
            nombre.setText(alumno.getNombre());
            ap_Pat.setText(alumno.getAp_Paterno());
            ap_Mat.setText(alumno.getAp_Materno());
            fecha.setText(alumno.getFecha_NacimientoString());
            guardar.setText("Actualizar");
        }
        return view;
    }



    public void setupView()
    {
        guardar=(Button)view.findViewById(R.id.btnGuardar);
        fecha=(EditText)view.findViewById(R.id.etFecha);
        nombre=(EditText)view.findViewById(R.id.etNombre);
        ap_Pat=(EditText)view.findViewById(R.id.etAp_Paterno);
        ap_Mat=(EditText)view.findViewById(R.id.etAp_Mat);
        mostrar=(Button)view.findViewById(R.id.btnMostrar);
        fecha.setInputType(InputType.TYPE_NULL);

        fecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c= Calendar.getInstance();
                int dia,mes,año;
                if(alumno!=null)
                    c.setTime(alumno.getFecha_Nacimiento());
                dia=c.get(Calendar.DAY_OF_MONTH);
                mes=c.get(Calendar.MONTH);
                año=c.get(Calendar.YEAR);


                DatePickerDialog datePickerDialog=new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        fecha.setText(i2+"/"+(i1+1)+"/"+i);
                    }
                },año,mes,dia);

                datePickerDialog.show();

            }
        });

        mostrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();
            }
        });

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if((nombre.getText().toString().length()>0)&&(ap_Pat.getText().toString().length()>0)&&(ap_Mat.getText().toString().length()>0)&&(fecha.getText().toString().length()>0))
                {
                    Alumno  alumno1=new Alumno(nombre.getText().toString(),ap_Pat.getText().toString(),ap_Pat.getText().toString(),dateFecha(fecha.getText().toString()));
                    if(alumno!=null)
                        alumno1.setId(alumno.getId());

                    bdManager.insertAlumno(alumno1);
                    mListener.updateLista();
                    if(alumno!=null)
                        Toast.makeText(getActivity(),"Alumno Actualizado Correctamente: \n"+alumno1.obtenerNombre(),Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(),"Alumno Guardado Correctamente: \n"+alumno1.obtenerNombre(),Toast.LENGTH_SHORT).show();

                    dismiss();
                }
                else
                    Toast.makeText(getActivity(),"Llene todos los CAMPOS ",Toast.LENGTH_SHORT).show();






            }
        });
    }

    public Date dateFecha(String fecha)
    {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaDate = null;
        try {
            fechaDate = formato.parse(fecha);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return fechaDate;
    }




    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }




    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
       public  void onFragmentInteraction(Uri uri);

        public void updateLista();
    }
}
