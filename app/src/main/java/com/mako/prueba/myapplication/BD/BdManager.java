package com.mako.prueba.myapplication.BD;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mako.prueba.myapplication.clases.Alumno;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Makofave on 16/09/2019.
 */

public class BdManager {

    public static final String Table_Alumno = "alumnos";

    public static final String Id_Alumno = "id";
    public static final String Nombre = "nombre";
    public static final String Ap_Paterno = "appaterno";
    public static final String Ap_Materno = "apmaterno";
    public static final String Fecha_Nac = "fechanac";

    public static final String Create_Table_Alumno=" create table "+Table_Alumno+" ("
            +Id_Alumno+ " integer primary key autoincrement, "
            +Nombre+" text,"
            +Ap_Paterno+" text,"
            + Ap_Materno+" text,"
            + Fecha_Nac+" text )";

    private BdHelper helper;
    private SQLiteDatabase bd;

    public BdManager(Context context)
    {
        helper=new BdHelper(context);
        bd=helper.getWritableDatabase();
    }


    private ContentValues generarContentUser(Alumno alumno)
    {
        ContentValues valores=new ContentValues();
        valores.put(Nombre,alumno.getNombre());
        valores.put(Ap_Paterno,alumno.getAp_Paterno());
        valores.put(Ap_Materno,alumno.getAp_Materno());
        valores.put(Fecha_Nac,alumno.getFecha_NacimientoString());
        return valores;
    }

    public void insertAlumno(Alumno alumno)
    {
        //si tiene una configuracion registrada, se actualiza.
        if(alumno.getId()>0)
        {
            bd.execSQL("UPDATE "+Table_Alumno+" SET " +
                    Nombre+"='"+alumno.getNombre()+"'," +
                    Ap_Paterno+"='"+alumno.getAp_Paterno()+"'," +
                    Ap_Materno+"='"+alumno.getAp_Materno()+"'," +
                    Fecha_Nac+"='"+alumno.getFecha_NacimientoString()+"' " +
                    "WHERE "+Id_Alumno+"='"+alumno.getId()+"' ");
        }
        //si no tiene, crea uno nuevo.
        else
            bd.insert(Table_Alumno,null,generarContentUser(alumno));


    }

    public List<Alumno> listaCap(){

        String cadenaSQL="SELECT * FROM "+Table_Alumno+" ORDER BY "+Id_Alumno+" DESC";
        List<Alumno> alumnos=new ArrayList<>();


        Cursor c=bd.rawQuery(cadenaSQL,null);
        int j=0,k=0;
        j=c.getCount();

        if(c.moveToFirst())
        {
            do {
                Alumno alumno=new Alumno(c.getString(1).toString(),
                        c.getString(2).toString(),
                        c.getString(3).toString(),
                        c.getString(4).toString());
                alumno.setId(c.getInt(0));
                alumnos.add(alumno);
            }while(c.moveToNext());




        }

        return alumnos;
    }

    public boolean eliminarAlumno(String id)
    {
        return bd.delete(Table_Alumno,Id_Alumno+"=?",new String[]{id})>0;
    }

    public int getCountTableAlumnos(){

        String cadenaSQL="SELECT * FROM "+Table_Alumno;
        Cursor c=bd.rawQuery(cadenaSQL,null);
        return c.getCount();

    }
}
